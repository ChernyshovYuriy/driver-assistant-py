import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

import cv2


class MouseEventHandler:
    def __init__(self, debug=False):
        print("MouseEventHandler class, debug:", debug)
        self.is_mouse_right_btn_down = False
        self.observer = []

    def add_observer(self, observer):
        self.observer.append(observer)

    def on_mouse_event(self, event, x, y, flags, param):
        # print("Mouse event:", event, " x:", x, ", y:", y, " flags:", flags, " param:", param)
        if event == cv2.EVENT_LBUTTONDOWN:
            self.is_mouse_right_btn_down = True
        if event == cv2.EVENT_LBUTTONUP:
            if self.is_mouse_right_btn_down:
                self.is_mouse_right_btn_down = False
                print(" - mouse clicked")
                for observer in self.observer:
                    observer.execute(x, y)
