import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

from src.main.ui.road_sign import RoadSign
from src.main.tensorflow.graph import Graph
from src.main.mouse_event_handler import MouseEventHandler
from src.main.ui.button import Button
from src.main.image_utils import save_image, get_file_name_as_cur_time
from src.main.input_scale import InputScale320240, InputScale640480
from src.main.shape_detector import Shape
from src.main.shape_processor import contour_to_image

import argparse
import datetime
import time

import cv2
import imutils
from imutils.video import VideoStream

from src.main.shape_detector import ShapeDetector

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--picamera", type=int, default=-1,
                help="whether or not the Raspberry Pi camera should be used")
ap.add_argument("-fs", "--fullscreen", type=int, default=-1,
                help="whether or not use a Full Screen mode")
ap.add_argument("-tdd", "--traindatadir", type=str, default="",
                help="path to the directory that contain data to use for training")
ap.add_argument("-d", "--debug", type=int, default=-1,
                help="whether or not use a Debug mode")
args = vars(ap.parse_args())

debug = args["debug"] == 1

project_root = dirname(dirname(dirname(abspath(__file__))))
print("Project root:", project_root)

print("OpenCV:", cv2.__version__)
print("imutils is cv2:", imutils.is_cv2())
print("debug mode:", debug)

project_root = dirname(dirname(dirname(abspath(__file__))))
print("Project root:", project_root)
train_data_dir = args["traindatadir"]
print("Train data dir:", train_data_dir)

start_time = time.time()
current_time = time.time()
input_scale = InputScale640480()
shape_detector = ShapeDetector(input_scale, debug)
frame_name = "Eye On The road"
ts = time.time()
frame = None
predicted_index = None
is_close_btn_clicked = False
close_button = Button()
road_sign = RoadSign()
mouse_event_handler = MouseEventHandler()
mouse_event_handler.add_observer(close_button)


# initialize the video stream and allow the camera sensor to warm up
vs = VideoStream(src=0, usePiCamera=args["picamera"] > 0).start()
time.sleep(2.0)


graph = Graph(train_data_dir)
graph.train()


def close_btn_listener():
    print("Close Btn Clicked")
    global is_close_btn_clicked
    is_close_btn_clicked = True

close_button.add_listener(close_btn_listener)

overlay = cv2.imread(project_root + "/resources/stop_sign_overlay.png", 1)
# overlay = imutils.resize(overlay, width=32)

# loop over the frames from the video stream
while True:
    current_time = time.time()
    if current_time - start_time < (1 / 24):
        continue
    start_time = current_time

    # grab the frame from the threaded video stream and resize it
    # to have a maximum width of 640 pixels
    frame = vs.read()
    frame = imutils.resize(frame, width=input_scale.get_video_width())
    image = frame.copy()

    red, contours = shape_detector.process(image)

    predicted_index = None
    for contour in contours:
        start = time.time()
        if contour[1] == Shape.hexagon \
                or contour[1] == Shape.ellipse \
                or contour[1] == Shape.triangle:
            cv2.drawContours(frame, [contour[0]], -1, (0, 255, 0), 2)
            detected_image = contour_to_image(image, contour[0], debug)
            # scaled_image = imutils.resize(detected_image, width=input_scale.get_pre_process_roi_width())
            predicted_index = graph.predict(cv2.cvtColor(detected_image, cv2.COLOR_BGR2RGB))
            save_image(project_root + "/output/detected", get_file_name_as_cur_time(), detected_image)

        if debug:
            print("Time to process:", (time.time() - start))

    # draw the timestamp on the frame
    timestamp = datetime.datetime.now()
    display_value = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
    cv2.putText(frame, display_value, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

    # make frame full screen
    if args["fullscreen"] > 0:
        cv2.namedWindow(frame_name, cv2.WND_PROP_FULLSCREEN)
        cv2.setWindowProperty(frame_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    close_button.init(frame, 20, 20, input_scale.get_video_width() - 30, 10, (255, 0, 0))
    close_button.create()

    # apply the overlay
    road_sign.set_frame(frame)
    if predicted_index is not None and len(predicted_index) >= 1:
        print("Predicted label:", predicted_index[0])
        if predicted_index[0] == 22:
            road_sign.handle_stop_sign()

    # show the frame
    cv2.imshow(frame_name, frame)

    cv2.setMouseCallback(frame_name, mouse_event_handler.on_mouse_event)

    key = cv2.waitKey(1) & 0xFF

    if debug and key == ord("s"):
        save_image(project_root + "/output", "screenshot.jpg", frame)

    # if the `q` key was pressed, break from the loop
    if key == ord("q") or is_close_btn_clicked:
        graph.stop()
        break

# do a bit of cleanup
cv2.destroyAllWindows()
vs.handle_stop_sign()
