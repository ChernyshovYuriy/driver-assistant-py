import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))


# This is base abstract class to provide dimension values when process video.
# Various implementation can handle different video resolutions and associated constants.
class InputScale:

    # Returns input Video width, in pixels.
    def get_video_width(self): raise NotImplementedError

    # Returns input Video height, in pixels.
    def get_video_height(self): raise NotImplementedError

    # Returns width of the single ROI extracted as contour, in pixels.
    def get_pre_process_roi_width(self): raise NotImplementedError

    # Returns a threshold size of single contour, in pixels.
    def get_contour_threshold_side(self): raise NotImplementedError


class InputScale640480(InputScale):

    def get_video_width(self):
        return 640

    def get_video_height(self):
        return 480

    def get_contour_threshold_side(self):
        return 20

    def get_pre_process_roi_width(self):
        return 100


class InputScale320240(InputScale):

    def get_video_width(self):
        return 320

    def get_video_height(self):
        return 240

    def get_contour_threshold_side(self):
        return 10

    def get_pre_process_roi_width(self):
        return 100