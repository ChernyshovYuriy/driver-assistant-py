import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(dirname(abspath(__file__))))))

import skimage.transform
import numpy as np
import skimage.transform
import skimage.data
import tensorflow as tf
from src.main.tensorflow import utils


class Graph:
    def __init__(self, train_data_dir, debug=False):
        print("Graph class, debug:", debug)
        self.debug = debug
        self.train_data_dir = train_data_dir
        self.session = None
        self.is_ready = False
        self.session = None
        self.predicted_labels = None
        self.images_ph = None
        self.train_img_size = 32

    def is_ready(self):
        return self.is_ready

    def train(self):
        print("Start Graph training ...")
        images, labels = utils.load_data(self.train_data_dir)

        if self.debug:
            print("Unique Labels: {0}\nTotal Images: {1}".format(len(set(labels)), len(images)))
            for image in images[:5]:
                print("shape: {0}, \tmin: {1}, \tmax: {2}".format(image.shape, image.min(), image.max()))

        # Resize images
        images32 = [skimage.transform.resize(image, (self.train_img_size, self.train_img_size))
                    for image in images]

        if self.debug:
            for image in images32[:5]:
                print("shape: {0}, \tmin: {1}, \tmax: {2}".format(image.shape, image.min(), image.max()))

        labels_a = np.array(labels)
        images_a = np.array(images32)

        if self.debug:
            print("labels: ", labels_a.shape, "\nimages: ", images_a.shape)

        # Create a graph to hold the model.
        graph = tf.Graph()

        # Create model in the graph.
        with graph.as_default():
            # Placeholders for inputs and labels.
            self.images_ph = tf.placeholder(
                tf.float32, [None, self.train_img_size, self.train_img_size, 3], name="images_ph"
            )
            labels_ph = tf.placeholder(tf.int32, [None], name="labels_ph")

            # Flatten input from: [None, height, width, channels]
            # To: [None, height * width * channels] == [None, 3072]
            images_flat = tf.contrib.layers.flatten(self.images_ph)

            # Fully connected layer.
            # Generates logits of size [None, 62]
            logits = tf.contrib.layers.fully_connected(images_flat, 62, tf.nn.relu)

            # Convert logits to label indexes (int).
            # Shape [None], which is a 1D vector of length == batch_size.
            self.predicted_labels = tf.argmax(logits, 1)

            # Define the loss function.
            # Cross-entropy is a good choice for classification.
            loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits, labels_ph))

            # Create training op.
            train = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

            # And, finally, an initialization op to execute before training.
            init = tf.global_variables_initializer()

            if self.debug:
                print("images_flat: ", images_flat)
                print("logits: ", logits)
                print("loss: ", loss)
                print("predicted_labels: ", self.predicted_labels)

            print("Graph trained")

            self.start(graph, init, train, loss, self.images_ph, images_a, labels_ph, labels_a)

    def start(self, graph, init, train, loss, images_ph, images_a, labels_ph, labels_a):
        print("Session start")
        # Create a session to run the graph we created.
        self.session = tf.Session(graph=graph)

        # First step is always to initialize all variables.
        # We don't care about the return value, though. It's None.
        _ = self.session.run([init])

        for i in range(20):
            _, loss_value = self.session.run([train, loss],
                                             feed_dict={images_ph: images_a, labels_ph: labels_a})
            if self.debug:
                if i % 10 == 0:
                    print("Loss: ", loss_value)

        self.is_ready = True
        print("Session started")

    def stop(self):
        # Close the session. This will destroy the trained model.
        if self.session is not None:
            self.session.close()

    def predict(self, image):
        # start = time.time()
        # Run predictions against the full test set.
        predicted = self.session.run([self.predicted_labels],
                                     feed_dict={
                                         self.images_ph: [
                                             skimage.transform.resize(image, (self.train_img_size, self.train_img_size))
                                         ]
                                     }
                                     )[0]
        # print("Time of session run:", (time.time() - start) * 1000, " ms")

        # print(test_labels_selected)
        return predicted
