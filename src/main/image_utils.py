import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

import datetime
import cv2
import os


def get_file_name_as_cur_time():
    ts = datetime.datetime.now()
    return "{}.jpg".format(ts.strftime("%Y-%m-%d_%H-%M-%S"))


def save_image(dir_name, file_name, image):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)

    p = os.path.sep.join((dir_name, file_name))

    # save the file
    cv2.imwrite(p, image)
