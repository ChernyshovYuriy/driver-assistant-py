import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))


from enum import Enum


class Shape(Enum):
    undefined = 0
    triangle = 1
    square = 2
    rectangle = 3
    pentagon = 4
    hexagon = 5
    ellipse = 6
