import sys
import time
from cv2 import createCLAHE, split, equalizeHist
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

import cv2
import numpy as np

from src.main.shape import Shape

#     [B, G, R]       [B, G, R]
# min [0, 0, 127] max [86, 64, 255]
# min [0, 0, 72]  max [13, 255, 255]
red_lower = np.array([0, 0, 125], dtype="uint8")
red_upper = np.array([115, 115, 255], dtype="uint8")

# These masks estimated by:
# https://bitbucket.org/ChernyshovYuriy/redcolorthreasholds/src
# Lower mask (0-10)
red_lower_lm = np.array([0, 70, 50])
red_lower_um = np.array([10, 255, 255])
# Upper mask (170-180)
red_upper_lm = np.array([165, 70, 50])
red_upper_um = np.array([180, 255, 255])


class ShapeDetector:
    def __init__(self, input_scale, debug=False):
        print("ShapeDetector class, debug:", debug)
        self.debug = debug
        self.input_scale = input_scale

    def process(self, image):
        # norm_image = self.normalize(image)
        # norm_image = self.adjust_gamma(norm_image)
        red_components_image = self.extract_red(image)
        return red_components_image, self.process_contours(red_components_image)

    def adjust_gamma(self, image):
        if self.debug:
            start = time.time()

        # build a lookup table mapping the pixel values [0, 255] to
        # their adjusted gamma values
        inv_gamma = 1.0 / 1
        table = np.array([((i / 255.0) ** inv_gamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
        # apply gamma correction using the lookup table
        gamma_image = cv2.LUT(image, table)

        if self.debug:
            print("Gamma adj time:", (time.time() - start))
            cv2.imshow("Gamma", gamma_image)

        return gamma_image

    def normalize(self, image):
        if self.debug:
            start_time = time.time()

        algorithm = -1

        if algorithm == -1:
            image_rgb = image

        if algorithm == 0:
            # convert RGB color image to Lab
            image_lab = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)
            # extract the L channel
            # now we have the L image in "list[0]"
            list = cv2.split(image_lab)

            # apply the CLAHE algorithm to the L channel
            clahe = createCLAHE(clipLimit=4.0, tileGridSize=(4, 4))
            dst = clahe.apply(list[0])

            # merge the the color planes back into an Lab image
            list[0] = dst.copy()
            cv2.merge(list, image_lab)
            # convert back to RGB
            image_rgb = cv2.cvtColor(image_lab, cv2.COLOR_Lab2BGR)
        if algorithm == 1:
            image_ycc = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)
            channels = split(image_ycc)
            equalizeHist(channels[0], channels[0])
            cv2.merge(channels, image_ycc)
            image_rgb = cv2.cvtColor(image_ycc, cv2.COLOR_YCrCb2BGR)

        if self.debug:
            if algorithm != -1:
                print("Colors normalization time:", (time.time() - start_time))
            # cv2.imshow("Equalized", image_rgb)

        return image_rgb

    def extract_red(self, image):
        if self.debug:
            start_time = time.time()

        algorithm = 2

        if algorithm == 0:
            global red_lower
            global red_upper

            red_image = cv2.inRange(image, red_lower, red_upper)
            red_image = cv2.GaussianBlur(red_image, (3, 3), 0)

        if algorithm == 1:
            red_image = image
            pass

        if algorithm == 2:
            global red_lower_lm
            global red_lower_um
            global red_upper_lm
            global red_upper_um

            img_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
            mask0 = cv2.inRange(img_hsv, red_lower_lm, red_lower_um)
            mask1 = cv2.inRange(img_hsv, red_upper_lm, red_upper_um)

            # Join masks
            mask = mask0 | mask1

            # Set output img to zero everywhere except mask
            # Bitwise-AND mask and original image
            red_image = cv2.bitwise_and(image, image, mask=mask)

            red_image = cv2.cvtColor(red_image, cv2.COLOR_BGR2GRAY)

        if self.debug:
            print("Red color extraction time:", (time.time() - start_time))
            # show red components
            cv2.imshow("Red contours", red_image)

        return red_image

    def process_contours(self, image):
        if self.debug:
            start_time = time.time()

        # find contours in the image
        cv2.imshow("TEST", image)
        _, contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]

        side_threshold_px = self.input_scale.get_contour_threshold_side()
        result = []

        if self.debug:
            print("Contours:", len(contours))

        # loop over the contours
        for contour in contours:
            width, height = self.get_contour_dimensions(contour)
            area = width * height
            if width < side_threshold_px or height < side_threshold_px:
                continue
            shape = self.detect(contour, area)
            if shape == Shape.hexagon or shape == Shape.ellipse:
                if self.debug:
                    print("Shape:", shape)
                result.append((contour, shape))

        if self.debug:
            print("Process contours time:", (time.time() - start_time))

        return result

    def detect(self, contour, area):
        if self.debug:
            start_time = time.time()

        # initialize the shape name and approximate the contour
        shape = Shape.undefined
        peri = cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, 0.02 * peri, True)
        if self.debug:
            print("Approx:", len(approx))

        # if the shape is a triangle, it will have 3 vertices
        if len(approx) == 3:
            shape = Shape.triangle

        # if the shape has 4 vertices, it is either a square or
        # a rectangle
        elif len(approx) == 4:
            # compute the bounding box of the contour and use the
            # bounding box to compute the aspect ratio
            (x, y, w, h) = cv2.boundingRect(approx)
            ar = w / float(h)

            # a square will have an aspect ratio that is approximately
            # equal to one, otherwise, the shape is a rectangle
            shape = Shape.square if 0.95 <= ar <= 1.05 else Shape.rectangle

        # if the shape is a pentagon, it will have 5 vertices
        # elif len(approx) == 8:
        #     shape = Shape.hexagon

        # otherwise, we assume the shape is a circle
        elif len(approx) == 8 or self.is_ellipse(contour):
            shape = Shape.ellipse

        # if self.debug:
            # print("Shape:", shape)
            # print("Detect shape time:", (time.time() - start_time))
            # print("Edges:", len(approx), "\tname:", shape, "\tarea: ", area)
        # return the name of the shape
        return shape

    def is_ellipse(self, contour):
        x_min = float("inf")
        x_max = 0
        y_min = float("inf")
        y_max = 0
        for point in contour:
            dot = point[0]
            x = dot[0]
            y = dot[1]
            if x > x_max:
                x_max = x
            if x < x_min:
                x_min = x
            if y > y_max:
                y_max = y
            if y < y_min:
                y_min = y
        width = x_max - x_min
        height = y_max - y_min
        a = width >> 1
        b = height >> 1

        delta = 0.2
        fail_counter = 0
        for point in contour:
            dot = point[0]
            x = dot[0] - x_min
            y = dot[1] - y_min
            result = self.process_point(a, b, x, y)
            # print("Res:", result)
            if result < 1 - delta or result > 1 + delta:
                fail_counter += 1
        if fail_counter <= ((len(contour) * 10) / 100):
            if self.debug:
                print("Shape is ellipse")
            return True
        if self.debug:
            print("Shape is not ellipse")
        return False

    @staticmethod
    def process_point(a, b, x, y):
        x_tmp = x - a
        y_tmp = y - b
        return ((x_tmp * x_tmp) / (a * a)) + ((y_tmp * y_tmp) / (b * b))

    @staticmethod
    def get_contour_dimensions(contour):
        x_min = float("inf")
        x_max = 0
        y_min = float("inf")
        y_max = 0
        for point in contour:
            dot = point[0]
            x = dot[0]
            y = dot[1]
            if x > x_max:
                x_max = x
            if x < x_min:
                x_min = x
            if y > y_max:
                y_max = y
            if y < y_min:
                y_min = y
        width = x_max - x_min
        height = y_max - y_min
        return [width, height]
