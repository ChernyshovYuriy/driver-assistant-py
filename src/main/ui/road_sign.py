import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(dirname(abspath(__file__))))))

import cv2

project_root = dirname(dirname(dirname(dirname(abspath(__file__)))))
STOP_SIGN = cv2.imread(project_root + "/resources/stop_sign_overlay.png")


class RoadSign:
    def __init__(self, debug=False):
        print("RoadSign class, debug:", debug)
        self.frame = None

    def set_frame(self, frame):
        self.frame = frame

    def handle_stop_sign(self):
        global STOP_SIGN

        cv2.addWeighted(STOP_SIGN, 1.0, self.frame, 1.0, 0.0, self.frame)

        # lines = [
        #     [77, 25],
        #     [55, 3],
        #     [25, 3],
        #     [3, 25],
        #     [3, 55],
        #     [25, 77],
        #     [55, 77],
        #     [77, 55],
        # ]
        # pts = np.array(lines, np.int32)
        # pts = pts.reshape((-1, 1, 2))
        # sign = cv2.fillPoly(self.frame, [pts], (0, 0, 255))