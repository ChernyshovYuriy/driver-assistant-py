import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(dirname(abspath(__file__))))))

import cv2
import numpy as np


class Button:
    def __init__(self, debug=False):
        print("Button class, debug:", debug)
        self.frame = None
        self.width = 10
        self.height = 10
        self.x = 10
        self.y = 10
        self.color = (255, 0, 0)
        self.listener = None

    def init(self, frame, width, height, x, y, color):
        self.frame = frame
        self.width = width
        self.height = height
        self.x = x
        self.y = y
        self.color = color

    def add_listener(self, listener):
        self.listener = listener

    def execute(self, x, y):
        if self.x <= x <= self.x + self.width and self.y <= y <= self.y + self.height:
            self.listener()

    def create(self):
        lines = []
        for i in range(self.y, self.y + self.height):
            lines.append([self.x, i])
            lines.append([self.x + self.width, i])
        pts = np.array(lines, np.int32)
        pts = pts.reshape((-1, 1, 2))
        button = cv2.polylines(self.frame, [pts], True, self.color, 1)
