import sys
from os.path import dirname, abspath

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

import cv2


def contour_to_image(image, contour, debug=False):
    x, y, w, h = cv2.boundingRect(contour)
    image_to_save = image[y:y + h, x:x + w]

    if debug:
        print("Contour x:", x, " y:", y, " w:", w, " h:", h)

    return image_to_save


def crop_stop_sign(image):
    # Calculate the width and height of the image
    height = len(image)
    width = len(image[0])
    height_percentage = 25
    width_percentage = 3
    crop_height = height * height_percentage / 100
    crop_width = width * width_percentage / 100
    # Crop from [y:h, x:w]
    image = image[
            crop_height:crop_height + (height - crop_height * 2),
            crop_width:crop_width + (width - crop_width * 2)
            ]
    return image

