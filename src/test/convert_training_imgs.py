import os
import csv
import cv2
from os.path import dirname, abspath

project_root = dirname(dirname(dirname(abspath(__file__))))
print("Project root:", project_root)

label_dir = os.path.join("/home/yurii/dev/BelgiumTSC_Training/00021")
file_names = [os.path.join(label_dir, f) for f in os.listdir(label_dir)]

print("Files:", len(file_names))

rows = {}

for file_name in file_names:
    if file_name.endswith(".csv"):
        print(" cvs file:", file_name)
        with open(file_name) as file:
            reader = csv.reader(file, delimiter=';', quoting=csv.QUOTE_NONE)
            for row in reader:
                print(row)
                if row[0] == "Filename":
                    continue
                rows[row[0]] = [int(row[3]), int(row[4]), int(row[5]) - int(row[4]), int(row[6]) - int(row[4])]

for file_name in file_names:
    if file_name.endswith(".ppm"):
        print(" img file:", file_name)
        index = file_name.rfind("/")
        crop_data = rows[file_name[index + 1:]]
        print("    details:", crop_data)

        img = cv2.imread(file_name)
        # [y: y + h, x: x + w]
        crop_img = img[crop_data[1]:crop_data[1] + crop_data[3], crop_data[0]:crop_data[0] + crop_data[2]]
        cv2.imwrite(project_root + "/" + file_name[index + 1:len(file_name) - 4] + ".jpg", crop_img)
