import sys
from os.path import dirname, abspath

from src.main.shape import Shape
from src.main.tensorflow.graph import Graph

sys.path.append(dirname(dirname(dirname(abspath(__file__)))))

import cv2
import imutils
import time

from src.main.image_utils import save_image, get_file_name_as_cur_time
from src.main.input_scale import InputScale640480, InputScale320240
from src.main.shape_detector import ShapeDetector
from src.main.shape_processor import contour_to_image

project_root = dirname(dirname(dirname(abspath(__file__))))
print("Project root:", project_root)

train_data_dir = "/home/yurii/dev/BelgiumTSC_Training"
print("Train data dir:", train_data_dir)

image = cv2.imread(project_root + "/resources/stop_sign_16.jpg", 1)
input_scale = InputScale640480()

height, width = image.shape[:2]
if height > width:
    image = imutils.resize(image, height=input_scale.get_video_height())
else:
    image = imutils.resize(image, width=input_scale.get_video_width())

shape_detector = ShapeDetector(input_scale, True)

graph = Graph(train_data_dir)
# graph.train()

start = time.time()

red, contours = shape_detector.process(image)

window_name = "Test Image"
cv2.namedWindow(window_name)

for contour in contours:
    detected_image = contour_to_image(image, contour[0], True)
    scaled_image = imutils.resize(detected_image, width=input_scale.get_pre_process_roi_width())

    cv2.drawContours(image, [contour[0]], -1, (0, 255, 0), 2)
    save_image(project_root + "/output", "scaled.jpg", scaled_image)
    save_image(project_root + "/output/detected", get_file_name_as_cur_time(), detected_image)
    if contour[1] == Shape.hexagon or contour[1] == Shape.ellipse:
        print("Shape detected")

print("Time to process:", (time.time() - start))

cv2.imshow(window_name, image)
cv2.waitKey(0)

cv2.destroyAllWindows()
