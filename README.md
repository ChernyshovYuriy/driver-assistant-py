# README #

This project use [OenCV](http://opencv.org/), [PyImageSearch](http://www.pyimagesearch.com/) and [TensorFlow](https://www.tensorflow.org/) libraries to detect and recognize road signs. It use Python 3 as base language.

Graph that is built by TensorFlow API uses data located at [German Traffic Sign Recognition Benchmark (GTSRB)](http://benchmark.ini.rub.de/)

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* One of the parameter that accepts by **main.py** is **"--traindatadir"**. Because the data used for the training is huge (more then 130 GB) the path to the directory set up as input argument. These data obtained from GTSRB.
* Configuration
* Dependencies

### Who do I talk to? ###

* Repo owner and admin **Yurii Chernyshov** (chernyshov.yuriy@gmail.com) | [LinkedIn](https://www.linkedin.com/in/yuriy-chernyshov-2706221b/)